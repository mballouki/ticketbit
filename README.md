# Ticket:bit


## Initialisation des cartes :

  1. Flasher le programme **admin_carte.py** sur une carte micro:bit, elle deviendra la carte admin.
  2. Flasher le programme **client_carte.py** sur toutes les autres cartes micro:bit, ce sera les cartes clients.
  3. Equiper de batteries les cartes clients

## Mise en place des ordinateurs :

  1. Pour l'adimistrateur:
     * Commencer par brancher la carte admin sur l'ordinateur administrateur.
     * Lancer le programme **file_attente_admin.py**.
     * Sélectionner le dossier (nous recommendons un dossier partagé dans toute l'entreprise) où sera stocké la base de donnée, **data_base_filedattente.db**.
  2. Pour les clients :
     * Lancer le programme **file_attente_client.py** sur l'ordinateur d'accueil.
     * Sélectionner le dossier où est stocké la base de donnée, renseigné dans la partie administrateur.
     * Mettez les cartes clients à disposition.
  3. Pour les guichets :
     * Lancer le programme **file_attente_guichet.py** sur les ordinateurs des guichetiers.
     * Sélectionner le dossier où est stockée la base de donnée, renseigné dans la partie administrateur.




#### Félicitation, vous avez fini l'initialisation. Vous pouvez dès à présent commencer à utiliser le projet Tiket:bit. 

## Les actions :

  * Carte admin :
    * Aucune action n'est a effectué ;
  * Carte client :
      * Pour l'activer, appuyer sur un bouton (A ou B indiféramment) après qu'un client ait été enregistré ;
      * Une fois activé, appuyer sur un bouton (A ou B indiféramment) pour être renseigné de votre place dans la file d'attente, du nombre de personnes devant vous ;
  * Administrateur :
      * Créer vos files d'attentes (pour vos différents services) via le bouton "ajouter une file d'attente" ;
      * Supprimer des files d'attentes via le bouton "enlever une file d'attente" ;
      * Conseil, faire cela avant l'ouverture de l'établissement ;
  * Clients :
      * Quand les clients arrivent, ils devront:
        * S'enregistrer ;
        * Indiquer leur nom ;
        * Sélectionner leur service parmis ceux actuellement ouverts ;
        * Prendre et activer leur carte ;
        * Se diriger en salle d'attente et attendant d'être appelé via sa carte client ;
  * Guichetier :
      * Les guichetiers ont juste à appuyer sur le bouton "appeler la personne suivante" quand besoin se fait.

