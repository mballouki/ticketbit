# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import filedialog
import sqlite3
import serial
import sys


port = "COM3"      #port de l'USB
baud = 115200 #même vitesse que pour la carte
s = serial.Serial(port) #ouverture du port
s.baudrate = baud #configure vitesse d'échange

couleurFile_fond = "#00234A"
couleurFile = "#0D335A"

couleur_nom_file = "#253d4e"
couleurClient = "#1D3D56"
couleurbouton = "#00204B"

base_de_donnee = "data_base_fileattente.db"

def Recherche_de_la_base_de_donnee():
    fenetre_temp = Tk()
    fenetre_temp.title("")
    global base_de_donnee
    base_de_donnee = filedialog.askdirectory(initialdir = "/", title = "Selectionez le dossier contenant la base de donnée") + "/data_base_fileattente.db"
    fenetre_temp.destroy()

Recherche_de_la_base_de_donnee()

#=========================================================================================================================================
class FenetreAdmin(Tk):

    def __init__(self) :
        super().__init__()
        self.attributes('-fullscreen', True)
        self.title("Admin")
        self.update()

        self.width = self.winfo_width()
        self.height = self.winfo_height()

        self.cadreliste = Frame(self, bg = "gray40")        #les files
        self.cadre1 = Frame(self, bg = "gray40")            #boutons bas
        self.cadre0 = Frame(self, bg = "gray40")            #boutons haut
        self.cadreliste.place(x=0, y=self.height//24, width=self.width,height=self.height*19//24)
        self.cadre1.place(x=0, y=self.height*5//6, width=self.width,height=self.height//6)
        self.cadre0.place(x=0, y=0, width=self.width,height=self.height//24)

        self.ecrans_liste = []

        self.frame_bouton_enleverfileattente = Frame(self.cadre1, borderwidth=2,bg = couleurFile, padx = 2, pady = 4)
        self.frame_bouton_ajouterfileattente = Frame(self.cadre1, borderwidth=2,bg = couleurFile, padx = 2, pady = 4)

        self.frame_bouton_enleverfileattente.pack(expand = 1, fill = "both", side = "left")
        self.frame_bouton_ajouterfileattente.pack(expand = 1, fill = "both", side = "left")

        self.bouton_enleverfileattente = Button(self.frame_bouton_enleverfileattente, text="enlever une file d'attente",  command=self.enlever_file , bg=couleurbouton,font=("Courier", 30),fg = "white", bd = 0)
        self.bouton_ajouterfileattente = Button(self.frame_bouton_ajouterfileattente, text="ajouter une file d'attente",  command= self.ajouter_file, bg=couleurbouton,font=("Courier", 30),fg = "white",bd = 0)



        self.frame_bouton_destruction = Frame(self.cadre0, borderwidth=2,bg = couleurFile, padx = 1, pady = 2)
        self.frame_bouton_tout_enlever = Frame(self.cadre0, borderwidth=2,bg = couleurFile, padx = 1, pady = 2)
        self.frame_bouton_vider_files = Frame(self.cadre0, borderwidth=2,bg = couleurFile, padx = 1, pady = 2)

        self.frame_bouton_destruction.pack(expand = 1, fill = "both", side = "left")
        self.frame_bouton_tout_enlever.pack(expand = 1, fill = "both", side = "left")
        self.frame_bouton_vider_files.pack(expand = 1, fill = "both", side = "left")





        self.bouton_destruction = Button(self.frame_bouton_destruction, text="quitter",  command= self.detruire, bg = couleurbouton, font=("Courier", 28),fg = "white",bd = 0)
        self.bouton_tout_enlever = Button(self.frame_bouton_tout_enlever, text="tout enlever",  command= self.creer_table, bg = couleurbouton, font=("Courier", 28),fg = "white",bd = 0)
        self.bouton_vider_files = Button(self.frame_bouton_vider_files, text="vider les files",  command= self.vider_files, bg = couleurbouton, font=("Courier", 28),fg = "white",bd = 0)



        self.bouton_destruction.pack(expand = 1, fill = "both", side = "left")
        self.bouton_tout_enlever.pack(expand = 1, fill = "both", side = "left")
        self.bouton_vider_files.pack(expand = 1, fill = "both", side = "left")


        self.bouton_ajouterfileattente.pack(expand = 1, fill = "both", side = "left")
        self.bouton_enleverfileattente.pack(expand = 1, fill = "both", side = "right")

        self.Affichage()


    def creer_table(self) :
        creer_table()
        self.Affichage()

    def vider_files(self) :

        vider_files()
        self.Affichage()

    def quitter(self) :
        self.quit()

    def ajouter_file(self) :
        FenetrePopUp_ajouter_file_attente(self)
        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()

        cursor.execute("SELECT * FROM files_attente")
        res = cursor.fetchall()
        print("files :")
        for row in res :
            print(row)
        print("----------")

        cursor.execute("SELECT * FROM guichets")
        res = cursor.fetchall()
        print("guichets :")
        for row in res :
            print(row)
        print("----------")


        cursor.execute("SELECT * FROM clients")
        res = cursor.fetchall()
        print("clients")
        for row in res :
            print(row)
        print("----------")


        connection.commit()
        connection.close()




    def Affichage(self) :
        for e in self.ecrans_liste :
            e.oubli()

        self.ecrans_liste = []
        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()

        cursor.execute("SELECT * FROM files_attente")
        files = cursor.fetchall()
        for row in files :
            self.ecrans_liste.append(FileAttente(self.cadreliste, row[0]))

        connection.commit()
        connection.close()


    def enlever_file(self) :
        FenetrePopUp_enlever_file_attente(self)

    def detruire(self) :
        destruction(self)

#=========================================================================================================================================

def destruction(elt):
    elt.destroy()


class Personne() :
    def __init__ (self,emplacement, indice, nom):

        self.nom = nom
        self.indice = indice
        self.emplacement = emplacement

        self.a_afficher = Afficher(self.nom, self.indice, self.emplacement)


    def destroy (self):
        self.a_afficher.detruire()



class Afficher ():

    def __init__(self, personne, place, emplacement) :
        """une classe qui affiche les classes Personne : indice + nom"""
        self.emplacement = emplacement
        self.personne = StringVar()
        self.placeaffiche = StringVar()
        self.place = place + 1

        #prendre le nom et la place
        self.placeaffiche.set( str(self.place))
        self.personne.set(personne)

        #les mettre dans des Stingvar
        self.truc_affiche = StringVar()
        self.truc_affiche.set(str(self.placeaffiche.get()) +" - " + self.personne.get())

        #afficher
        self.frame = Frame(self.emplacement, bg = couleurFile_fond, padx= 2, pady = 2 )
        self.pers_a_afficher = Label(self.frame)
        self.pers_a_afficher.configure(text = self.truc_affiche.get(), font=("Courier", 30),bg = couleurClient , fg = "white" )
        self.frame.pack(fill = 'x')
        self.pers_a_afficher.pack(fill = 'x')



    def detruire(self):

        self.pers_a_afficher.destroy()




class FileAttente(Frame) :

    def __init__ (self, ecran, id):
        super().__init__(ecran,bg = couleurFile_fond, padx= 2, pady = 4 )
        """pour gérer l'affichage de la file; ajouter pour y mettre un client"""

        self.frame = Frame(self, borderwidth=2,bg = couleurFile )
        self.frame_label = Frame(self.frame, bg = couleurFile_fond, padx= 2, pady = 2 )

        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()



        cursor.execute("SELECT * FROM files_attente WHERE ID =  ?", (id,))
        nom = cursor.fetchall()[0][1]

        self.pack(side = 'left', expand = 1, fill = 'both')
        self.frame.pack(side = 'left', expand = 1, fill = 'both')
        self.frame_label.pack(fill = "x")
        Label(self.frame_label,font=("Courier", 30),text=nom,  bg = couleur_nom_file, fg = "white").pack(fill = "x")

        self.id = id
        id = str(id)
        cursor.execute("SELECT * FROM clients WHERE guichet =  ?", (id,))
        result = cursor.fetchall()
        for i, row in enumerate(result) :
            Personne(self.frame, i, row[2])





        connection.commit()
        connection.close()


    def ajouter(self,nomUtilisateur) :
        #for ecran in self.ecrans :
        Personne(self, self.id, nomUtilisateur)

    def oubli (self) :
        self.pack_forget()



#==========================================================================================================




class FenetrePopUp_enlever_file_attente(Toplevel) :
    def __init__(self, master) :
        super().__init__()
        self.minsize(800, 450)
        self.title("enlever une file d'attente")
        self.frame = Frame(self, bg = couleurFile)
        self.frame.pack(fill = "both", expand= True)
        self.master = master


        Label(self.frame, text="quelle file souhaitez vous enlever ?", bg =couleurFile, fg = 'white', font =("Courrier", 30)).pack(side = TOP, expand = 1, fill = 'x')


        # créer la listbox

        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM files_attente")
        res = cursor.fetchall()
        self.lbx = Listbox(self.frame, bg = couleurbouton, fg = "white", highlightcolor = "black",selectbackground = couleurbouton, height = res[0], width = 25, font = ("Courier", 30))


        cursor.execute("SELECT * FROM files_attente")
        res = cursor.fetchall()
        for i,row in enumerate(res) :
            self.lbx.insert(i, row[1])


        self.lbx.bind("<<ListboxSelect>>",  self.updateLabel)
        self.lbx.select_set(0)

        self.lbx.pack(expand = 1)

        connection.commit()
        connection.close()
        self.lbl = Label(self.zoneentrees, textvariable=self.selected_borne)

    def updateLabel(self, event=None):
            """sert à changer de guichet"""
            try :
                line = self.lbx.curselection()[0]

                connection = sqlite3.connect(base_de_donnee)
                cursor = connection.cursor()

                cursor.execute("SELECT * FROM files_attente")

                res = cursor.fetchall()[line][0]

                cursor.execute("DELETE FROM files_attente WHERE id = ?", (res,))

                connection.commit()
                connection.close()
                self.master.Affichage()
                self.destroy()
            except :
                pass


class FenetrePopUp_ajouter_file_attente(Toplevel):

    def __init__(self, master) :
        super().__init__()
        self.minsize(800, 450)
        self.title("ajouter une file d'attente")
        self.cadre1 = Frame(self, bg = couleurFile)
        self.cadre1.pack(fill = "both", expand= True)
        self.master = master


        self.nomGuichet = StringVar()
        self.nomGuichet.set("entrez le nom de la file")     # texte par défaut affiché dans l'entrée

        self.nomGuichetFinal = StringVar()
        self.nomGuichetFinal.set("entrez le nom de la file")



        self.entree = Entry(self.cadre1, textvariable = self.nomGuichetFinal, width = 30, font = ("Courier", 20))
        self.entree.pack(expand = 1)


        self.entree.bind("<FocusIn>", self.Effacer)

        self.bouton = Button(self.cadre1, text="cliquez ici pour créer la file",  command= self.Nouv_file, bg=couleurbouton,font=("Courier", 30),fg = "white")
        self.bouton.pack(expand = 1)

        self.lbl = Label(self.zoneentrees, textvariable=self.selected_borne)
    def Effacer(self, event = None) :
        self.nomGuichetFinal.set("")

    def Nouv_file(self) :
        """vérifie le nom puis crée une file"""
        if self.nomGuichetFinal.get() != self.nomGuichet.get() and self.nomGuichetFinal.get() != "" :
            if ajouter_file_attente(self.nomGuichetFinal.get()) :
                self.master.Affichage()
                self.destroy()
            else :
                id_fail = Label(self.cadre1, text="il y a trop de files ", bg = couleurFile, fg = "red", font = ("Courier", 20))
                id_fail.pack(fill = 'x', side = "bottom")
                self.after(1000, id_fail.destroy)







def creer_table ():

    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()


    #cursor.execute("DROP TABLE IF EXISTS clients")
    cursor.execute("DROP TABLE IF EXISTS clients")
    cursor.execute("CREATE TABLE IF NOT EXISTS clients(id INTEGER PRIMARY KEY AUTOINCREMENT, guichet INTEGER, nom TEXT)")

    cursor.execute("DROP TABLE IF EXISTS guichets")
    cursor.execute("CREATE TABLE IF NOT EXISTS guichets(id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT, file_attente INT)")

    cursor.execute("DROP TABLE IF EXISTS files_attente")
    cursor.execute("CREATE TABLE IF NOT EXISTS files_attente(id INTEGER PRIMARY KEY AUTOINCREMENT, nom TEXT)")


    connection.commit()
    connection.close()




def ajouter_file_attente(nom) :
    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()

    cursor.execute("SELECT COUNT(*) FROM files_attente ")
    res = cursor.fetchall()
    if res[0][0] < 6:

        cursor.execute("INSERT INTO files_attente(nom) VALUES(?)", (nom,))

        connection.commit()
        connection.close()
        return True

    else :
        connection.close()
        return False

def ajouter_elmt(guichet,nom) :

    cursor.execute("SELECT MAX(id) FROM clients", str(guichet))
    idmax = cursor.fetchall()[0] + 1

    message = bytes(f'1|{guichet}|{idmax}', 'utf-8')
    s.write(message)


    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()

    cursor.execute("Insert into clients(guichet, nom ) values (?,?)", (guichet, nom) )

    connection.commit()
    connection.close()


def enlever_elmt(guichet) :
    """enlève le premier élément de la database du guichet spécifié
    -> entrée : le numéro du guichet INT
    -> sortie : un tuple contenant :
                - l'Id INT
                - le guichet INT
                - le nom TEXT """

    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM clients WHERE guichet = ? ORDER BY id", str(guichet))
    resultat = cursor.fetchall()
    client = resultat[0]

    clientID = str(client[0])



    cursor.execute("DELETE FROM clients WHERE id = ?", (clientID,))

    connection.commit()
    connection.close()


    """message = bytes(f'2|{guichet}|{clientID}', 'utf-8')
    s.write(message)"""

    return(client)



def vider_files() :

    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()

    cursor.execute("DELETE FROM clients")

    connection.commit()
    connection.close()




def position_file_d_attente():
    data = str(s.readline())[2:-5]
    if data != "0":
        sys.stdout.write(str(data) + "\n")
        code, envoyeur, guichet = map(int, data.split("|"))
        if code == 1:

            connection = sqlite3.connect(base_de_donnee)
            cursor = connection.cursor()
            cursor.execute("SELECT id FROM clients WHERE guichet = ? ORDER BY id", (int(guichet),))
            resultat = cursor.fetchall()
            connection.commit()
            connection.close()

            sys.stdout.write(str(resultat) + "-" + str(envoyeur) + "\n")

            for i in range(len(resultat)):
                sys.stdout.write(str(int(resultat[i][0])) + "\n")
                if int(resultat[i][0]) == envoyeur:
                    sys.stdout.write("--\n")
                    message = bytes(f'3|{i}|{envoyeur}', 'utf-8')
                    s.write(message)




def maj():
    recevoir_message()
    position_file_d_attente()
    fenetreAdmin.after(200, maj)


def creer_table_message():
    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()

    #cursor.execute("DROP TABLE  IF EXISTS messages")
    cursor.execute("CREATE TABLE IF NOT EXISTS messages(id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT)")

    connection.commit()
    connection.close()


def recevoir_message() :

    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()

    cursor.execute("SELECT message FROM messages")
    res = cursor.fetchall()
    if res != [] :
        for row in res :
            code, guichet, client = row[0].split("|")
            if code == '1':
                cursor.execute("INSERT INTO clients (guichet, nom ) VALUES (?, ?)", (int(guichet), client))

                cursor.execute("SELECT id FROM clients WHERE guichet = ? AND nom = ?", (int(guichet), client))
                idclient = cursor.fetchall()[0][0]

                message = bytes(f'1|{guichet}|{idclient}', 'utf-8')
                s.write(message)

            if code == '2':
                cursor.execute("DELETE FROM clients WHERE id = ?", (int(client),))

                message = bytes(f'2|{guichet}|{client}', 'utf-8')
                s.write(message)



        cursor.execute("DELETE FROM messages")


        fenetreAdmin.after(500, fenetreAdmin.Affichage)

    cursor.execute("SELECT * FROM clients")
    res = cursor.fetchall()


    connection.commit()
    connection.close()
# ----- Programme principal : -----

creer_table()
creer_table_message()

fenetreAdmin = FenetreAdmin()

recevoir_message()

maj()
#fenetreAdmin.after(100, position_file_d_attente)

fenetreAdmin.mainloop()


s.close()