# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import filedialog
import sqlite3

couleurFile_fond = "#00234A"
couleurFile = "#0D335A"

couleur_nom_file = "#253d4e"
couleurClient = "#1D3D56"
couleurbouton = "#00204B"

def Recherche_de_la_base_de_donnee():
    """ crée une fenêtre pour rechercher la base de donnée dans le pc
        entrée : rien
        sortie : rien"""
    fenetre_temp = Tk()
    fenetre_temp.title("")
    global base_de_donnee
    base_de_donnee = filedialog.askdirectory(initialdir = "/", title = "Selectionez le dossier contenant la base de donnée") + "/data_base_fileattente.db"
    fenetre_temp.destroy()
    
Recherche_de_la_base_de_donnee()

#======================================================================================================================
class FenetreGuichet(Tk):
    
    def __init__(self,file) :
         """ crée une fenêtre pour le client. On peut y voir la liste des clients, les appeller ou quitter la fenêtre
        entrée : la file affiliée au guichet (int)
        sortie : rien"""
        super().__init__() 

        self.nom = file[1]
        self.fileId = file[0]

        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()

        cursor.execute("INSERT INTO guichets(nom, file_attente) VALUES (?,?)", (self.nom, self.fileId))
        cursor.execute("SELECT id FROM guichets WHERE nom = ? AND file_attente = ?", (self.nom, self.fileId))
        self.id = cursor.fetchall()[0][0]
        
        connection.commit()
        connection.close()
        

        self.attributes('-fullscreen', True)
        self.title(self.nom)
        self.update()

        self.width = self.winfo_width()
        self.height = self.winfo_height()
        
        self.personne_actuelle = StringVar()
        self.personne_actuelle.set("personne")
        
        self.frame_file = Frame(self, bg = couleurFile)
        self.frame_file.pack(side = "left", fill = "both",expand = 1)

        self.frame_autre = Frame(self, bg = couleurFile_fond)
        self.frame_autre.pack(side = "right", fill = "both")
        
        self.bouton_destruction = Button(self.frame_autre, text="quitter",  command= self.detruire, bg=couleurbouton,font=("Courier", 30),fg = "white")
        self.bouton_destruction.pack(fill = "both", side = "top")

        self.label = Label(self.frame_autre, text = "vous êtes actuellement avec :", font=("Courier", 26), bg = couleurFile_fond, fg = "white", padx = 70)
        self.label.pack(expand = 1, side = "top",)
        
        self.client = Label(self.frame_autre, textvariable = self.personne_actuelle, font=("Courier", 26), bg =couleurFile_fond, fg = "white", padx = 70)
        self.client.pack(expand = 1, side = "top")
        
        self.bouton_appeler_client = Button(self.frame_autre, text="appeler la personne suivante",  command= self.defiler , bg=couleurbouton,font=("Courier", 30),fg = "white")
        self.bouton_appeler_client.pack(expand = 1, fill = "both")

        self.ecrans_liste = []

        self.Affichage()
        
    def defiler(self):
        """ enlève le dernier élément de sa file d'attente (appelle la fonction (enlever_elt))
        entrée : self
        sortie : rien"""
        personne = enlever_elmt(self.fileId)
        self.chang_pers(personne)
    
    def chang_pers(self, pers) :
        """ met à jour la personne actuelle avec la dernière personne affichée
        entrée : self, le nom de la personne appellée(str)
        sortie : rien"""
        self.personne_actuelle.set(pers)

    def Affichage(self) :
        """rafraîchit l'interface
        entrée : self
        sortie : rien"""
        for e in self.ecrans_liste :
            e.oubli()

        self.ecrans_liste = []
        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()

        cursor.execute("SELECT * FROM files_attente WHERE id = ?", (self.fileId,))
        files = cursor.fetchall()
        for row in files :
            self.ecrans_liste.append(FileAttente(self.frame_file, row[0]))

        connection.commit()
        connection.close()

    def detruire(self) :
        """détruit la fenêtre
        entrée : self
        sortie : rien"""
        destruction(self)
#=========================================================================================================================================

def destruction(elt):
    """détruit la fenêtre
        entrée : la fenêtre à détruire(objet Tk)
        sortie : rien"""
    elt.destroy()










class Personne() :
    
    def __init__ (self,emplacement, indice, nom):
        """crée une personne possédant un nom, un emplacement et uine place dans la file.
            Il servira à afficher un label de son nom dans sa file (fonction Afficher)
        entrée : self, file d'attente affiliée (int), place dans la file(int), nom (str)
        sortie : rien"""

        self.nom = nom
        self.indice = indice
        self.emplacement = emplacement

        self.a_afficher = Afficher(self.nom, self.indice, self.emplacement)


    def destroy (self):
        """détruit l'objet
        entrée : self
        sortie : rien"""
        self.a_afficher.detruire()



class Afficher ():

    def __init__(self, personne, place, emplacement) :
        """Il servira à afficher un label de son nom et de sa place dans sa file affiliée
        entrée : self, file d'attente affiliée (int), place dans la file(int), nom (str)
        sortie : rien"""
        self.emplacement = emplacement
        self.personne = StringVar()
        self.placeaffiche = StringVar()
        self.place = place + 1

        #prendre le nom et la place
        self.placeaffiche.set( str(self.place))
        self.personne.set(personne)

        #les mettre dans des Stingvar
        self.truc_affiche = StringVar()
        self.truc_affiche.set(str(self.placeaffiche.get()) +" - " + self.personne.get())

        #afficher
        self.frame = Frame(self.emplacement, bg = couleurFile_fond, padx= 2, pady = 2 )
        self.pers_a_afficher = Label(self.frame)
        self.pers_a_afficher.configure(text = self.truc_affiche.get(), font=("Courier", 30),bg = couleurClient , fg = "white" )
        self.frame.pack(fill = 'x')
        self.pers_a_afficher.pack(fill = 'x')



    def detruire(self):
        """détruit l'objet
        entrée : self
        sortie : rien"""

        self.pers_a_afficher.destroy()




class FileAttente(Frame) :

    def __init__ (self, ecran,Id):
        super().__init__(ecran,bg = couleurFile_fond, padx= 2, pady = 4 )
        """Crée une Frame correspondant à une file sur une base de donnée. 
        entrée : self, Frame/Fenetre sur laquelle afficher la Frame (objet Frame/ objet Tk), id de la file dans la base de donnée (int)
        sortie : rien"""

        self.frame = Frame(self, borderwidth=2,bg = couleurFile )
        self.frame_label = Frame(self.frame, bg = couleurFile_fond, padx= 2, pady = 2 )

        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()



        cursor.execute("SELECT * FROM files_attente WHERE ID =  ?", (Id,))
        nom = cursor.fetchall()[0][1]
        
        self.pack(side = 'left', expand = 1, fill = 'both')
        self.frame.pack(side = 'left', expand = 1, fill = 'both')
        self.frame_label.pack(fill = "x")
        Label(self.frame_label,font=("Courier", 30),text=nom,  bg = couleur_nom_file, fg = "white").pack(fill = "x")

        self.id = Id
        Id = str(Id)
        cursor.execute("SELECT * FROM clients WHERE guichet =  ?", (Id,))
        result = cursor.fetchall()
        for i, row in enumerate(result) :
            Personne(self.frame, i, row[2])


        


        connection.commit()
        connection.close()


    def ajouter(self,nomUtilisateur) :
        """ajoute une personne dans la file d'attente
        entrée : self, nom de l'utilisateur(str)
        sortie : rien"""
        #for ecran in self.ecrans :
        Personne(self, self.id, nomUtilisateur)

    def oubli (self) :
        """sert à enlever l'affichage de la Frame
        entrée : self
        sortie : rien"""
        self.pack_forget()


#==================================================================================================================


class FenetreGuichetSeul(Tk) :
    def __init__(self) :
        super().__init__()
        """crée une fenêtra dans laquelle on choisit la file dont on veut devenir guichetier. La fenêtre se fermera seule 
        et créera la fenêtre principale du guichetier
        entrée : self
        sortie : rien"""
        détruit la fenêtre
        self.minsize(800, 450)
        self.title("ajouter une file d'attente")
        self.frame = Frame(self, bg = couleurFile)
        self.frame.pack(fill = "both", expand= True)


        Label(self.frame, text="de quelle file souhaitez vous être le guichet ?", bg =couleurFile, fg = 'white', font =("Courrier", 30)).pack(side = TOP, expand = 1, fill = 'x')

        # créer la listbox
        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()

        cursor.execute("SELECT COUNT(*) FROM files_attente")
        res = cursor.fetchall()
        self.lbx = Listbox(self.frame, bg = couleurFile_fond, fg = "white", highlightcolor = "black",selectbackground = couleurFile_fond, height = res, width = 25, font = ("Courier", 30))


        cursor.execute("SELECT * FROM files_attente")
        res = cursor.fetchall()
        for i,row in enumerate(res) :
            print(row)
            self.lbx.insert(i, row[1])

        self.lbx.bind("<<ListboxSelect>>",  self.updateLabel)
        self.lbx.select_set(0)

        self.lbx.pack(expand = 1)

        connection.commit()
        connection.close()
       

    def updateLabel(self, event=None):
            """choisit la file associée et met l'indice de la file choisie dans res 
                entrée : self
                sortie : rien"""
            try :
                line = self.lbx.curselection()[0]

                connection = sqlite3.connect(base_de_donnee)
                cursor = connection.cursor()
    
                cursor.execute("SELECT * FROM files_attente ")

                global res
                res = cursor.fetchall()[line]
                
                connection.commit()
                connection.close()

                self.destroy()
            except :
                pass





#==================================================================================================================       








def enlever_elmt(guichet) :
    """enlève le premier élément de la database du guichet spécifié
    -> entrée : le numéro du guichet INT
    -> sortie : un tuple contenant :
                - l'Id INT
                - le guichet INT
                - le nom TEXT """

    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()

    cursor.execute("SELECT * FROM clients WHERE guichet = ? ORDER BY id", str(guichet))
    
    resultat = cursor.fetchall()
    client_a_enlever = resultat[0]
    idclient = str(client_a_enlever[0])

    message = f"2|{fenetreguichet.id}|{idclient}"
    cursor.execute("INSERT INTO messages(message) VALUES(?)",(message,))
    print(idclient)

    connection.commit()
    connection.close()

    return(client_a_enlever[2])





def maj():
    """met à jour l'interface de la fenêtre client toutes les 5 secondes
        entrée : rien
        sortie : rien"""
    fenetreguichet.Affichage()
    fenetreguichet.after(2000, maj)

# ----- Programme principal : -----





#creer_table()

fenetreTemp = FenetreGuichetSeul()

fenetreTemp.mainloop()

fenetreguichet = FenetreGuichet(res)
maj()
fenetreguichet.mainloop()