"""Radio code : "envoyeur|destinataire|code|informationSuplémentaire"
1:  admin -> clients : un client s'est inscrit, il doit se lié à une carte
2:  client -> admin  : le client s'est lié avec cette carte

3:  client -> admin  : demande du nombre de persones devant lui
4:  admin -> client  : nombre de persones devant le client

5:  admin -> client  : appelle du client à son guichet
6:  client -> admin  : accusé de réception du 5
"""


from microbit import *
import radio

radio.config(group=2)
radio.on()
uart.init(baudrate=115200)


def indice(tab, elt):
    """fonction indice() renvoie l'indice d'un élément dans un tableau (ou -1 si l'élément n'y est pas)

    entrées:
        tab : le tableau
        elt : l'élément dont on veut connaitre la position

    sortie:
        l'indice de l'élément dans le tableau
    """
    for i in range(len(tab)):
        if tab[i][0] == elt:
            return i
    return -1




def pos(client, file):
    """permet de renvoyer la position d'un client dans sa file d'attente en communiquant avec l'ordinateur
    entrées: client INT, id du client
             file INT, numéro de la file du client

    sortie:  position INT, la position actuelle du client dans sa file
             (100 si la conection avec l'ordinateur est impossible)"""

    for i in range(1000):
        print("1|" + str(client) + "|" + str(file))
        if uart.any():
            msg = str(uart.read())[2:-1]
            code, position, client = map(int, msg.split("|")[:3])
            if code == 3:
                return position

    return 100


aPasseTab = []
enregistreTab = []



while True:
    print("0") # pour ne pas que l'ordinateur attende indéfiniment un message / signifie R.A.S.
    display.scroll(".", 10) # permet de vérifier le bon fonctionnement de la carte et de ralentir les communications pour ne pas les faire couper par sécurité
    message = radio.receive()
    # stoque en un string le message sur le réseau bleuthooth, les messages recu interressant sont ceux envoyés par la carte admin
    if message:  # si un message est reçu des autres cartes
        envoyeur, destinataire, code, contenu = map(int, message.split("|"))
        # tous ces éléments correspondent a l'ordre du message et tout ce qu'on envoie. Ils sont séparé d'un '|', et sont aplliqués d'une fonction par la fonction map
        if code == 2:
            enregistreTab.pop() #reception du lien carte-client
        if code == 3:
            position = pos(envoyeur, contenu)            # envoie le nombre de personne devant le client l'ayant demandé
            radio.send("0|" + str(envoyeur) + "|4|" + str(position))
        if code == 6:
            aPasseTab.pop(indice(aPasseTab, envoyeur))

    if uart.any(): # si un message est reçu de l'ordinateur
        uart_mess = str(uart.read())
        uart_clean = uart_mess[2:-1]

        code, file, client = map(int, uart_clean.split("|"))

        if code == 1:
            enregistreTab.append((client, file)) # permet de faire enregistrer un client

        if code == 2:
            aPasseTab.append((client,file)) #pour lui, file = numéro du guichet # permet d'appeler un client





    for i in range(len(aPasseTab)):
        radio.send("0|" + str(aPasseTab[i][0]) + "|5|" + str(aPasseTab[i][1])) # appelle les clients devant l'être


    for c in enregistreTab:
        radio.send("0|"+str(c[0])+"|1|"+str(c[1])) # signale qu'un client doit pendre une carte
