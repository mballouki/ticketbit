# -*- coding: utf-8 -*-
from tkinter import *
from tkinter import filedialog
import sqlite3

couleurFile_fond = "#00234A"
couleurFile = "#0D335A"

couleur_nom_file = "#253d4e"
couleurClient = "#1D3D56"
couleurbouton = "#00204B"

def Recherche_de_la_base_de_donnee():
    """ crée une fenêtre pour rechercher la base de donnée dans le pc
        entrée : rien
        sortie : rien"""
    fenetre_temp = Tk()
    fenetre_temp.title("")
    global base_de_donnee
    base_de_donnee = filedialog.askdirectory(initialdir = "/", title = "Selectionez le dossier contenant la base de donnée") + "/data_base_fileattente.db"
    fenetre_temp.destroy()
    
Recherche_de_la_base_de_donnee()


class FenetreGuichet (Tk):
    def __init__(self) :
        super().__init__() 
        """ crée une fenêtre pour le client. On peut y voir la liste des clients et s'enregister
        entrée : rien
        sortie : rien"""
        #caractéristiques de la fenêtre
        self.attributes('-fullscreen', True)
        self.update()

        self.width = self.winfo_width() 
        self.height = self.winfo_height() 
        
        self.title("Sale d'attente")
        self.minsize(self.width,self.height)
        self.update()

        #créer les frames
        self.conteneur_ecrans = Frame(self)
        self.zoneentrees = Frame(self, bg = couleurFile)



        self.ecrans_liste = []
        self.Affichage()
        
        self.boutons =  Frame(self.zoneentrees, bg = couleurFile)

        self.bouton_du_bas = Button(self.zoneentrees, text="cliquez ici pour vous enregistrer",  command= self.interface2, bg=couleurbouton,font=("Courier", 30),fg = "white", padx= 2, pady = 4,bd = 0)
        self.bouton_interface1 = Button(self.zoneentrees, text="cliquez ici pour revenir",  command= self.interface1, bg = couleurbouton, fg = "white",font=("Courier", 30), padx= 2, pady = 4,bd = 0)

        
        
        # créer la listbox
        self.creer_lbx()

        #créer l'entrée
        self.nomUtilisateur = StringVar()
        self.nomUtilisateur.set("entrez votre nom")     # texte par défaut affiché dans l'entrée
        
        self.nomUtilisateur2 = StringVar()
        self.nomUtilisateur2.set("entrez votre nom") 

        self.entree = Entry(self.boutons, textvariable = self.nomUtilisateur, width = 40, font = ("Courier", 30))
        self.entree.bind("<Return>", self.SetNom)   
        self.entree.bind("<FocusIn>", self.Effacer) 
        
        self.selected_borne = StringVar()
        self.interface1()


    def creer_lbx(self) :
        """ crée une listbox dans la fenêtre contenant les différentes files d'attente
        entrée : self
        sortie : rien"""

        self.lbx = Listbox(self.boutons, bg = couleurFile_fond, fg = "white", highlightcolor = "black",selectbackground = couleurFile_fond, height = len(self.ecrans_liste), width = 25, font = ("Courier", 30))

        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()
        
        cursor.execute("SELECT * FROM files_attente")
        result = cursor.fetchall()
        for i, row in enumerate(result) :
            self.lbx.insert(i, row[1])  

        self.lbx.bind("<<ListboxSelect>>",  self.updateLabel)
        self.lbx.select_set(0)

        connection.commit()
        connection.close()  



    def Effacer(self, event = None) :
        """ efface le nom d'utilisateur dans l'entry
        entrée : self
        sortie : rien"""
        self.nomUtilisateur.set("")



    def updateLabel(self, event=None):
            """sert à changer de guichet puis appelle SetNom()
            entrée : rien
            sortie : rien"""
            try :
                line = self.lbx.curselection()[0]
                self.selected_borne.set(line)
                self.def_guichet(line)
                self.SetNom()
            except :
                pass
    

    def interface1(self) :
        """met en place la première interface
        entrée : self
        sortie : rien"""
        self.conteneur_ecrans.place(x=0, y=0, width=self.width,height=self.height*4//5)
        self.zoneentrees.place(x=0, y=self.height*4//5, width=self.width,height=self.height//5)
        

        for i in range(len(self.ecrans_liste)) :
            self.ecrans_liste[i].pack(side = LEFT, fill =BOTH, expand = True)


        self.boutons.pack_forget()

        self.bouton_du_bas.pack(fill = BOTH, expand = True)

        #self.bouton_sajouter.pack_forget()
        #self.bouton2.pack_forget()
        self.bouton_interface1.pack_forget()

        
        #self.lbl.pack_forget()
        self.lbx.pack_forget()


        self.entree.pack_forget()

    
        

    def interface2 (self) :
        """met en place la seconde interface
        entrée : self
        sortie : rien"""
        self.zoneentrees.place(x=0, y=0,width = self.width, height = self.height)
        self.conteneur_ecrans.place_forget()
        



        #self.bouton2.pack()
        
        self.boutons.pack(expand = 1)

        self.lbx.pack(side = BOTTOM)
        self.entree.pack(side=LEFT)
        #self.bouton_sajouter.pack(expand =1, side = RIGHT)
        #self.bouton2.pack(expand = 0)

        

        self.bouton_du_bas.pack_forget()


        self.bouton_interface1.pack(side = BOTTOM, fill = BOTH, ipady = 40)
        
        #self.lbl.pack()
        
        
        self.guichet_actuel = None

        self.bouton_du_bas.focus_set()
        self.nomUtilisateur.set(self.nomUtilisateur2.get())

    

    def SetNom(self, event = None) :        
        """vérifie si tous les paramètres sont bien renseignés puis crée un utilisateur et remet l'interfacce 1
        entrée : self
        sortie : rien"""
        if self.guichet_actuel != None and self.nomUtilisateur.get() != self.nomUtilisateur2.get():
            if len(self.nomUtilisateur.get()) <= 22 :
                
                self.ajouter(self.nomUtilisateur.get())



                self.interface1()
                self.nomUtilisateur.set(self.nomUtilisateur2.get())
            else : 
                id_fail = Label(self.zoneentrees, text="le nom est trop long ", bg = "gray40", fg = "red")
                id_fail.pack(fill = 'x')
                self.after(1000, id_fail.destroy)


    def ajouter(self, nom) :
        """appelle la fonction ajouet_elmt(envoi d'un message à l'admin pour ajouter une nouvelle personne) puis rafraîchit l'interface
        entrée : self, nom (string)
        sortie : rien"""
        ajouter_elmt(self.guichet_actuel.id, nom)
        self.Affichage()



    def Affichage(self) :
        """rafraîchit l'interface
        entrée : self
        sortie : rien"""
        for e in self.ecrans_liste :
            e.oubli()
            
        self.ecrans_liste = []
        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()
        
        cursor.execute("SELECT * FROM files_attente")
        files = cursor.fetchall()
        for row in files :
            self.ecrans_liste.append(FileAttente(self.conteneur_ecrans, row[0]))

        connection.commit()
        connection.close()



    def def_guichet(self, nbr) :
        """prend en paramètre l'indice du guichet puis sélectionne le bon guichet
        entrée : self, nbr (int)
        sortie : rien"""
        self.guichet_actuel = self.ecrans_liste[nbr]





            
        

    
class Personne() :
    
    def __init__ (self,emplacement, indice, nom):
        """crée une personne possédant un nom, un emplacement et uine place dans la file.
            Il servira à afficher un label de son nom dans sa file (fonction Afficher)
        entrée : self, file d'attente affiliée (int), place dans la file(int), nom (str)
        sortie : rien"""

        self.nom = nom
        self.indice = indice
        self.emplacement = emplacement

        self.a_afficher = Afficher(self.nom, self.indice, self.emplacement)


    def destroy (self):
        """détruit l'objet
        entrée : self
        sortie : rien"""
        self.a_afficher.detruire()



class Afficher ():

    def __init__(self, personne, place, emplacement) :
        """Il servira à afficher un label de son nom et de sa place dans sa file affiliée
        entrée : self, file d'attente affiliée (int), place dans la file(int), nom (str)
        sortie : rien"""
        self.emplacement = emplacement
        self.personne = StringVar()
        self.placeaffiche = StringVar()
        self.place = place + 1

        #prendre le nom et la place
        self.placeaffiche.set( str(self.place))
        self.personne.set(personne)

        #les mettre dans des Stingvar
        self.truc_affiche = StringVar()
        self.truc_affiche.set(str(self.placeaffiche.get()) +" - " + self.personne.get())

        #afficher
        self.frame = Frame(self.emplacement, bg = couleurFile_fond, padx= 2, pady = 2 )
        self.pers_a_afficher = Label(self.frame)
        self.pers_a_afficher.configure(text = self.truc_affiche.get(), font=("Courier", 30),bg = couleurClient , fg = "white" )
        self.frame.pack(fill = 'x')
        self.pers_a_afficher.pack(fill = 'x')



    def detruire(self):
        """détruit l'objet
        entrée : self
        sortie : rien"""

        self.pers_a_afficher.destroy()




class FileAttente(Frame) :

    def __init__ (self, ecran,Id):
        super().__init__(ecran,bg = couleurFile_fond, padx= 2, pady = 4 )
        """Crée une Frame correspondant à une file sur une base de donnée. 
        entrée : self, Frame/Fenetre sur laquelle afficher la Frame (objet Frame/ objet Tk), id de la file dans la base de donnée (int)
        sortie : rien"""

        self.frame = Frame(self, borderwidth=2,bg = couleurFile )
        self.frame_label = Frame(self.frame, bg = couleurFile_fond, padx= 2, pady = 2 )

        connection = sqlite3.connect(base_de_donnee)
        cursor = connection.cursor()



        cursor.execute("SELECT * FROM files_attente WHERE ID =  ?", (Id,))
        nom = cursor.fetchall()[0][1]
        
        self.pack(side = 'left', expand = 1, fill = 'both')
        self.frame.pack(side = 'left', expand = 1, fill = 'both')
        self.frame_label.pack(fill = "x")
        Label(self.frame_label,font=("Courier", 30),text=nom,  bg = couleur_nom_file, fg = "white").pack(fill = "x")

        self.id = Id
        Id = str(Id)
        cursor.execute("SELECT * FROM clients WHERE guichet =  ?", (Id,))
        result = cursor.fetchall()
        for i, row in enumerate(result) :
            Personne(self.frame, i, row[2])


        


        connection.commit()
        connection.close()


    def ajouter(self,nomUtilisateur) :
        """ajoute une personne dans la file d'attente
        entrée : self, nom de l'utilisateur(str)
        sortie : rien"""
        #for ecran in self.ecrans :
        Personne(self, self.id, nomUtilisateur)

    def oubli (self) :
        """sert à enlever l'affichage de la Frame
        entrée : self
        sortie : rien"""
        self.pack_forget()

 
#♣=====================================================================================================
        
        
        
def ajouter_elmt(guichet,nom) :
    """crée les tables sur la base de donnée selectionnée (lors de Recherche_de_la_base_de_donnee()) si elles n'existent pas
        entrée : rien
        sortie : rien"""


    message = f"1|{guichet}|{nom}"

    
    connection = sqlite3.connect(base_de_donnee)
    cursor = connection.cursor()
    
    cursor.execute("INSERT INTO messages(message) VALUES(?)",(message,))

    

    connection.commit()
    connection.close()

        
def maj():
     """met à jour l'interface de la fenêtre client toutes les 5 secondes
        entrée : rien
        sortie : rien"""
    fenetreGuichet.Affichage()
    fenetreGuichet.after(2000, maj)
    
    
    
# ----- Programme principal : -----





connection = sqlite3.connect(base_de_donnee)
cursor = connection.cursor()

cursor.execute("DROP TABLE  IF EXISTS messages")
cursor.execute("CREATE TABLE IF NOT EXISTS messages(id INTEGER PRIMARY KEY AUTOINCREMENT, message TEXT)")

connection.commit()
connection.close()

fenetreGuichet = FenetreGuichet()


maj()

fenetreGuichet.mainloop()
